<?php
/**
 * @author Roman Strilenko
 * @email strell@strelldev.com
 * @diva-e
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'StrellDev_Cron',
    __DIR__
);
