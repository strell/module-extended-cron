<?php
/**
 * @author Roman Strilenko
 * @email strell@strelldev.com
 * @diva-e
 */
declare(strict_types=1);

namespace StrellDev\Cron\Api;

use Symfony\Component\Console\Output\OutputInterface;

interface JobsListDecoratorInterface
{
    /**
     * Decorate the list
     * @param array $listToDecorate
     * @return array
     */
    public function decorate(array $listToDecorate): array;
}
