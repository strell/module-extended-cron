<?php
/**
 * @author Roman Strilenko
 * @email strell@strelldev.com
 * @diva-e
 */
declare(strict_types=1);

namespace StrellDev\Cron\Api;

interface PatternBuilderInterface
{
    /**
     * Build the pattern
     * @param string $pattern
     * @return string
     */
    public function build(string $pattern): string;
}
