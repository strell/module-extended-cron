<?php
/**
 * @author Roman Strilenko
 * @email strell@strelldev.com
 * @diva-e
 */
declare(strict_types=1);

namespace StrellDev\Cron\Api;

interface CronJobsFilterInterface
{
    /**
     * Filter the crontab jobs
     * @param array $filterableList
     * @return array
     */
    public function filter(array $filterableList): array;
}
