<?php
/**
 * @author Roman Strilenko
 * @email strell@strelldev.com
 * @diva-e
 */
declare(strict_types=1);

namespace StrellDev\Cron\Api;

use Symfony\Component\Console\Output\OutputInterface;

interface RendererInterface
{
    /**
     * Provide the rendering
     * @param array $data
     * @param OutputInterface $output
     * @return RendererInterface
     */
    public function render(array $data, OutputInterface $output): RendererInterface;
}
