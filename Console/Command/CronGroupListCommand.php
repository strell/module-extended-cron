<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace StrellDev\Cron\Console\Command;

use Magento\Framework\App\ObjectManagerFactory;
use Magento\Framework\ObjectManagerInterface;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManager;
use StrellDev\Cron\Model\Application\Bootstrap;
use StrellDev\Cron\Model\Application\ListGroupsApp;
use StrellDev\Cron\Model\Filter\FilterByName;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Command for executing cron jobs
 */
class CronGroupListCommand extends Command
{
    /**
     * Input option name
     */
    const INPUT_OPTION_NAME = 'name';

    /**
     * Object manager factory
     *
     * @var ObjectManagerFactory
     */
    private $objectManagerFactory;

    /**
     * CronListCommand constructor.
     * @param ObjectManagerFactory $objectManagerFactory
     */
    public function __construct(
        ObjectManagerFactory $objectManagerFactory
    ) {
        $this->objectManagerFactory = $objectManagerFactory;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $options = [
            new InputOption(
                self::INPUT_OPTION_NAME,
                null,
                InputOption::VALUE_OPTIONAL,
                'Filer by name'
            )
        ];
        $this->setName('cron:group:list')
            ->setDescription('List All Cron Groups')
            ->setDefinition($options);
        parent::configure();
    }

    /**
     * Runs cron jobs if cron is not disabled in Magento configurations
     *
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $omParams = $_SERVER;
        $omParams[StoreManager::PARAM_RUN_CODE] = 'admin';
        $omParams[Store::CUSTOM_ENTRY_POINT_PARAM] = true;
        $objectManager = $this->objectManagerFactory->create($omParams);

        /** @var Bootstrap $applicationBootstrap */
        $applicationBootstrap = $objectManager->create(
            Bootstrap::class
        );

        $applicationBootstrap->execute();

        /** @var ObjectManagerInterface $bootstrappedObjectManager */
        $bootstrappedObjectManager = $objectManager->get(ObjectManagerInterface::class);

        if ($input->getOption(self::INPUT_OPTION_NAME)) {
            $bootstrappedObjectManager->get(FilterByName::class)->setSearchableValue(
                $input->getOption(self::INPUT_OPTION_NAME)
            );
        }

        $cronListApplication = $objectManager->create(
            ListGroupsApp::class,
            [
                'output'  => $output
            ]
        );
        $cronListApplication->launch();
    }
}
