<?php
/**
 * @author Roman Strilenko
 * @email strell@strelldev.com
 * @diva-e
 */
declare(strict_types=1);

namespace StrellDev\Cron\Model\Filter;

use StrellDev\Cron\Api\CronJobsFilterInterface;
use StrellDev\Cron\Exception\FilterException;

class FilterPool implements CronJobsFilterInterface
{
    /**
     * Filterers
     * @var array
     */
    private $filterers;

    /**
     * FilterPool constructor.
     * @param array $filterers
     */
    public function __construct(array $filterers = [])
    {
        $this->filterers = array_map(
            function ($executor) {
                if (!$executor instanceof CronJobsFilterInterface) {
                    throw new FilterException(
                        __('The filter executor does not implement "%s"', CronJobsFilterInterface::class)
                    );
                }

                return $executor;
            },
            $filterers
        );
    }

    /**
     * Filter using the filterer pool
     * @param array $filterableList
     * @return array
     */
    public function filter(array $filterableList): array
    {
        /** @var CronJobsFilterInterface $filterer */
        foreach ($this->filterers as $filterer) {
            $filterableList = $filterer->filter($filterableList);
        }

        return $filterableList;
    }
}
