<?php
/**
 * @author Roman Strilenko
 * @email strell@strelldev.com
 * @diva-e
 */
declare(strict_types=1);

namespace StrellDev\Cron\Model\Filter;

use StrellDev\Cron\Api\CronJobsFilterInterface;
use StrellDev\Cron\Api\PatternBuilderInterface;

abstract class AbstractFilterable implements CronJobsFilterInterface
{
    /**
     * Pattern builder interface
     * @var PatternBuilderInterface
     */
    protected $patternBuilder;

    /**
     * Searchable value
     * @var string
     */
    protected $searchableValue;

    /**
     * AbstractFilterable constructor.
     * @param PatternBuilderInterface $patternBuilder
     */
    public function __construct(PatternBuilderInterface $patternBuilder)
    {
        $this->patternBuilder = $patternBuilder;
    }

    /**
     * Provide the filtering
     * @param $item
     * @return mixed
     */
    abstract protected function doFilter($item): bool;

    /**
     * Seat a searchable value
     * @param string $searchableValue
     * @return AbstractFilterable
     */
    public function setSearchableValue(string $searchableValue): AbstractFilterable
    {
        $this->searchableValue = $searchableValue;
        return $this;
    }

    /**
     * Validate the value by pattern
     * @param string $value
     * @return bool
     */
    protected function validateUsingPattern(string $value): bool
    {
        return (bool)preg_match($this->patternBuilder->build($this->searchableValue), $value);
    }

    /**
     * Dont apply filtering
     * @param array $filterableList
     * @return array
     */
    public function filter(array $filterableList): array
    {
        return isset($this->searchableValue)
            ? array_filter(
                $filterableList,
                [
                    $this,
                    'doFilter'
                ]
            ): $filterableList;
    }
}
