<?php
/**
 * @author Roman Strilenko
 * @email strell@strelldev.com
 * @diva-e
 */
declare(strict_types=1);

namespace StrellDev\Cron\Model\Filter;

use StrellDev\Cron\Api\PatternBuilderInterface;

class PatternBuilder implements PatternBuilderInterface
{
    /**
     * Default quote pattern
     */
    const DEFAULT_QUOTE_PATTERN = '/\\.[](){}^$+-';

    /**
     * Pattern body
     */
    const PATTERN_BODY = '/^%s$/';

    /**
     * Quoted characters
     * @var array
     */
    private $quoteCharacters;

    /**
     * Escaped characters
     * @var array
     */
    private $escapedCharacters;

    /**
     * PatternBuilder constructor.
     * @param string $extraQuotePattern
     * @param array $extraQuotePatternArray
     */
    public function __construct(string $extraQuotePattern = '', array $extraQuotePatternArray = [])
    {
        $this->quoteCharacters = array_unique(
            array_merge(
                str_split(
                    self::DEFAULT_QUOTE_PATTERN
                        . $extraQuotePattern
                ),
                $extraQuotePatternArray
            )
        );

        $this->escapedCharacters = array_map(
            function ($character) {
                return '\\' . $character;
            },
            $this->quoteCharacters
        );
    }

    /**
     * Build the pattern
     * @param string $pattern
     * @return string
     */
    public function build(string $pattern): string
    {
        return sprintf(
            self::PATTERN_BODY,
            str_replace(
                array_merge($this->quoteCharacters, ['*']),
                array_merge($this->escapedCharacters, ['.*']),
                $pattern
            )
        );
    }
}
