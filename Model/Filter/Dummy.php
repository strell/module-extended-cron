<?php
/**
 * @author Roman Strilenko
 * @email strell@strelldev.com
 * @diva-e
 */
declare(strict_types=1);

namespace StrellDev\Cron\Model\Filter;

use StrellDev\Cron\Api\CronJobsFilterInterface;

class Dummy implements CronJobsFilterInterface
{
    /**
     * Dont apply filtering
     * @param array $filterableList
     * @return array
     */
    public function filter(array $filterableList): array
    {
        return $filterableList;
    }

}
