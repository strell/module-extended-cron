<?php
/**
 * @author Roman Strilenko
 * @email strell@strelldev.com
 * @diva-e
 */
declare(strict_types=1);

namespace StrellDev\Cron\Model\Filter;

use StrellDev\Cron\Api\CronJobsFilterInterface;

class FilterByGroup extends AbstractFilterable implements CronJobsFilterInterface
{
    /**
     * Filter by groups
     * @param $item
     * @return bool
     */
    protected function doFilter($item): bool
    {
        return $this->validateUsingPattern($item['group'] ?? '');
    }

}
