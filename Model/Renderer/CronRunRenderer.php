<?php
/**
 * @author Roman Strilenko
 * @email strell@strelldev.com
 * @diva-e
 */
declare(strict_types=1);
namespace StrellDev\Cron\Model\Renderer;


use StrellDev\Cron\Api\RendererInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CronRunRenderer implements RendererInterface
{
    /**
     * Empty renderer
     * @param array $data
     * @param OutputInterface $output
     * @return RendererInterface
     */
    public function render(array $data, OutputInterface $output): RendererInterface
    {
        $output->writeln(
            'The cron job was ran <info>successfully</info>'
        );
        $output->writeln(
            sprintf(
                '<info>%s</info> executor: <comment>%s::%s</comment>',
                $data['name'] ?? 'unknown',
                $data['instance'] ?? 'unknown',
                $data['method'] ?? 'unknown'
            )
        );
        return $this;
    }
}
