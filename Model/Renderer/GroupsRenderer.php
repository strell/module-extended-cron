<?php
/**
 * @author Roman Strilenko
 * @email strell@strelldev.com
 * @diva-e
 */
declare(strict_types=1);
namespace StrellDev\Cron\Model\Renderer;


use StrellDev\Cron\Api\RendererInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GroupsRenderer implements RendererInterface
{
    /**
     * Render the data lines
     * @param array $data
     * @param OutputInterface $output
     * @return RendererInterface
     */
    public function render(array $data, OutputInterface $output): RendererInterface
    {
        $output->writeln(
            sprintf(
                '<info>%s</info> jobs count: <comment>%d</comment>',
                $data['name'] ?? 'unknown',
                count($data['items'] ?? [])
            )
        );
        return $this;
    }
}
