<?php
/**
 * @author Roman Strilenko
 * @email strell@strelldev.com
 * @diva-e
 */
declare(strict_types=1);

namespace StrellDev\Cron\Model\Decorator;

use StrellDev\Cron\Api\JobsListDecoratorInterface;

class JobsFlattener implements JobsListDecoratorInterface
{
    /**
     * Decorate the jobs list
     * @param array $listToDecorate
     * @return array
     */
    public function decorate(array $listToDecorate): array
    {
        return array_merge(
            ...array_values(
                array_map(
                    function ($groupData, $groupKey) {
                        return array_map(
                            function ($item) use ($groupKey) {
                                return array_merge(
                                    $item,
                                    [
                                        'group' => $groupKey
                                    ]
                                );
                            },
                            $groupData
                        );
                    },
                    $listToDecorate,
                    array_keys($listToDecorate)
                )
            )
        );
    }
}
