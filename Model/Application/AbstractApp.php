<?php
/**
 * @author Roman Strilenko
 * @email strell@strelldev.com
 * @diva-e
 */
declare(strict_types=1);
namespace StrellDev\Cron\Model\Application;

use Magento\Cron\Model\ConfigInterface;
use Magento\Framework\App;
use Magento\Framework\AppInterface;
use StrellDev\Cron\Api\CronJobsFilterInterface;
use StrellDev\Cron\Api\JobsListDecoratorInterface;
use StrellDev\Cron\Api\RendererInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class AbstractApp implements AppInterface
{
    /**
     * Output instance
     * @var OutputInterface
     */
    protected $output;

    /**
     * Cron config instance
     * @var ConfigInterface
     */
    protected $cronConfig;

    /**
     * Cron job filter
     * @var CronJobsFilterInterface
     */
    protected $cronJobsFilter;

    /**
     * Renderer
     * @var RendererInterface
     */
    protected $renderer;

    /**
     * Jobs list decorator
     * @var JobsListDecoratorInterface
     */
    protected $jobsListDecorator;

    /**
     * ListApp constructor.
     * @param OutputInterface $output
     * @param ConfigInterface $cronConfig
     * @param CronJobsFilterInterface $cronJobsFilter
     * @param RendererInterface $renderer
     * @param JobsListDecoratorInterface $jobsListDecorator
     */
    public function __construct(
        OutputInterface $output,
        ConfigInterface $cronConfig,
        CronJobsFilterInterface $cronJobsFilter,
        RendererInterface $renderer,
        JobsListDecoratorInterface $jobsListDecorator
    )
    {
        $this->output = $output;
        $this->cronConfig = $cronConfig;
        $this->cronJobsFilter = $cronJobsFilter;
        $this->renderer = $renderer;
        $this->jobsListDecorator = $jobsListDecorator;
    }

    /**
     * Process the cron jobs list
     * @param array $cronJobsList
     * @return array
     */
    abstract protected function processCronJobsList(array $cronJobsList): array;

    /**
     * Process the cronjobs list
     * @return App\ResponseInterface|void
     */
    public function launch()
    {
        $cronJobs = $this->cronJobsFilter->filter(
            $this->jobsListDecorator->decorate(
                $this->cronConfig->getJobs()
            )
        );

        $cronJobs = $this->processCronJobsList($cronJobs);

        /** @var  $jocConfig */
        foreach ($cronJobs as $jocConfig) {
            $this->renderer->render($jocConfig, $this->output);
        }
    }

    /**
     * Catch the exception
     * @param App\Bootstrap $bootstrap
     * @param \Exception $exception
     * @return bool
     */
    public function catchException(App\Bootstrap $bootstrap, \Exception $exception)
    {
        return false;
    }
}
