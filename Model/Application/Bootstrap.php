<?php
/**
 * @author Roman Strilenko
 * @email strell@strelldev.com
 * @diva-e
 */
declare(strict_types=1);
namespace StrellDev\Cron\Model\Application;

use Magento\Framework\App;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\ObjectManager\ConfigLoaderInterface;
use Magento\Framework\ObjectManagerInterface;

class Bootstrap
{
    /**
     * Application state
     * @var State
     */
    private $state;

    /**
     * Object manager
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var App\AreaList
     */
    private $areaList;

    /**
     * Bootstrap constructor.
     * @param State $state
     * @param ObjectManagerInterface $objectManager
     * @param App\AreaList $areaList
     */
    public function __construct(
        State $state,
        ObjectManagerInterface $objectManager,
        App\AreaList $areaList
    )
    {
        $this->state = $state;
        $this->objectManager = $objectManager;
        $this->areaList = $areaList;
    }

    /**
     * Bootstrap the cron application
     * @return ListApp
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(): Bootstrap
    {
        $this->state->setAreaCode(Area::AREA_CRONTAB);
        /** @var ConfigLoaderInterface $configLoader */
        $configLoader = $this->objectManager->get(ConfigLoaderInterface::class);
        $this->objectManager->configure($configLoader->load(Area::AREA_CRONTAB));
        $this->areaList->getArea(Area::AREA_CRONTAB)->load(Area::PART_TRANSLATE);

        return $this;
    }
}
