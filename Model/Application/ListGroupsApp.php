<?php
/**
 * @author Roman Strilenko
 * @email strell@strelldev.com
 * @diva-e
 */
declare(strict_types=1);
namespace StrellDev\Cron\Model\Application;

use Magento\Framework\AppInterface;

class ListGroupsApp extends AbstractApp implements AppInterface
{
    /**
     * Process the cron jobs list
     * @param array $cronJobsList
     * @return array
     */
    protected function processCronJobsList(array $cronJobsList): array
    {
        $cronGroups = [];
        foreach ($cronJobsList as &$cronJobInfo) {
            $groupName = $cronJobInfo['group'] ?? 'no-group';
            if (!isset($cronGroups[$groupName])) {
                $cronGroups[$groupName] = [
                    'name'  => $groupName,
                    'items' => []
                ];
            }

            $cronGroups[$groupName]['items'][] = $cronJobInfo;
        }

        return $cronGroups;
    }
}
