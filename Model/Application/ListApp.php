<?php
/**
 * @author Roman Strilenko
 * @email strell@strelldev.com
 * @diva-e
 */
declare(strict_types=1);
namespace StrellDev\Cron\Model\Application;

use Magento\Framework\AppInterface;

class ListApp extends AbstractApp implements AppInterface
{
    /**
     * Do nothing, rehder the cron jobs list
     * @param array $cronJobsList
     * @return array
     */
    protected function processCronJobsList(array $cronJobsList): array
    {
        return $cronJobsList;
    }
}
