<?php
/**
 * @author Roman Strilenko
 * @email strell@strelldev.com
 * @diva-e
 */
declare(strict_types=1);
namespace StrellDev\Cron\Model\Application;

use Magento\Cron\Model\ConfigInterface;
use Magento\Framework\AppInterface;
use Magento\Framework\ObjectManagerInterface;
use StrellDev\Cron\Api\CronJobsFilterInterface;
use StrellDev\Cron\Api\JobsListDecoratorInterface;
use StrellDev\Cron\Api\RendererInterface;
use StrellDev\Cron\Exception\ApplicationException;
use Symfony\Component\Console\Output\OutputInterface;

class RunJob extends AbstractApp implements AppInterface
{
    /**
     * Object manager
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * RunJob constructor.
     * @param OutputInterface $output
     * @param ConfigInterface $cronConfig
     * @param CronJobsFilterInterface $cronJobsFilter
     * @param RendererInterface $renderer
     * @param JobsListDecoratorInterface $jobsListDecorator
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        OutputInterface $output,
        ConfigInterface $cronConfig,
        CronJobsFilterInterface $cronJobsFilter,
        RendererInterface $renderer,
        JobsListDecoratorInterface $jobsListDecorator,
        ObjectManagerInterface $objectManager
    ) {
        $this->objectManager = $objectManager;
        parent::__construct($output, $cronConfig, $cronJobsFilter, $renderer, $jobsListDecorator);
    }

    /**
     * Run the job
     * @param array $cronJobsList
     * @return array
     * @throws ApplicationException
     */
    protected function processCronJobsList(array $cronJobsList): array
    {
        if (count($cronJobsList) > 1) {
            throw new ApplicationException(
                __(
                    'There are more then 1 jobs matching the selection, matches: "%1"',
                    implode(
                        ', ',
                        array_map(
                            function (array $cronJobInfo) {
                                return $cronJobInfo['name'] ?? 'unknown';
                            },
                            $cronJobsList
                        )
                    )
                )
            );
        } elseif (!count($cronJobsList)) {
            throw new ApplicationException(__('There are no matches for specified cronjob pattern'));
        }

        reset($cronJobsList);
        $currentJob = current($cronJobsList);

        $this->output->writeln(sprintf('Starting <info>%s</info>', $currentJob['name'] ?? 'unknown'));
        $this->output->writeln(
            sprintf(
                'Using executor <info>%s::%s</info>',
                $currentJob['instance'] ?? 'unknown',
                $currentJob['method'] ?? 'unknown'
            )
        );

        $executor = $this->objectManager->create($currentJob['instance']);
        $executor->{$currentJob['method']}();

        return $cronJobsList;
    }
}
