<?php
/**
 * @author Roman Strilenko
 * @email strell@strelldev.com
 * @diva-e
 */
declare(strict_types=1);

namespace StrellDev\Cron\Exception;

use Magento\Framework\Exception\LocalizedException;

class BaseException extends LocalizedException
{

}
